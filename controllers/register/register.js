const handleRegister = (db, bcrypt) => (req, res) => {
const { username, password } = req.body;
  const hash = bcrypt.hashSync(password);
  console.log('hash', hash);
    db.transaction(trx => {
      trx.insert({
        username: username,
        password: hash
      })
      .into('users')
      .returning(['id','username'])
      .then(data => {
        const isValid = bcrypt.compareSync(password, hash)
        if(isValid){
          console.log(typeof(password), typeof(hash));
          console.log('valid');
        } else {
          console.log('not valid');
        }
        return res.json(data)
      })
      .then(trx.commit)
      .catch(trx.rollback)
    })
    .catch(err => res.status(400).json('unable to register'))
  }

    module.exports = {
      handleRegister
    }