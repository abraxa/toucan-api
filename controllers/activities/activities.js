const handleGetActivities = (db) => (req, res) => {
  db.select('*').from('activities')
    .then(data => {
        return res.json(data)
    })
    .catch(err => res.status(400).json('error in conecction'))
}

const handleGetActivitiesWhere = (db) => (req, res) => {
  console.log('req',req.params);
  db.select('*').from('activities')
  .where({iduser: req.params.iduser})
    .then(data => {
        return res.json(data)
    })
    .catch(err => res.status(400).json('error in conecction'))
}

const handleDeleteActivitiesWhere = (db) => (req, res) => {
  console.log('req',req.params, res.params, res.body, req.body);
  db.from('activities')

  .where({id: req.body.id})
  .del()
  .then(data => {
        return res.json(data)
  })
  .catch(err => res.status(400).json('error in conecction'))
}

const handlePostActivities = (db) => (req, res) => {
  const { iduser, name, timetype, status, resttime, inittime, dateact, generated, initializedtime } = req.body;
  db.transaction(trx => {
    trx.insert({
      iduser: iduser, 
      name: name, 
      timetype: timetype, 
      status: status, 
      resttime: resttime,
      inittime: inittime,
      dateact: dateact,
      generated: generated,
      initializedtime: initializedtime
    })
    .into('activities')
    .then(data => {
      res.json(data[0]);
    })
    .then(trx.commit)
    .catch(trx.rollback)
  })
}

const handleGetActivitiesId = (db) => (req, res) => {
const { id } = req.params;
  db.select('*').from('activities').where({id})
    .then(activitie => {
      if (activitie.length) {
        res.json(activitie[0])
      } else {
        res.status(400).json('Not found')
      }
    })
    .catch(err => res.status(400).json('error getting activitie'))
}

const handlePutActivities = (db, moment) => (req, res) => {
const { id } = req.body;
  console.log('req body', req.body);
  console.log('id', id);
  db('activities').where('id', '=', id)
  .update(req.body)
  .then(entries => {
    console.log('entries', entries);
    res.json('¡Cambio con éxito!');
  })
  .catch(err => res.status(400).json('unable to get entries'))
}

module.exports = {
  handleGetActivities: handleGetActivities,
  handlePostActivities: handlePostActivities,
  handleGetActivitiesId: handleGetActivitiesId,
  handlePutActivities: handlePutActivities,
  handleGetActivitiesWhere: handleGetActivitiesWhere,
  handleDeleteActivitiesWhere: handleDeleteActivitiesWhere
}