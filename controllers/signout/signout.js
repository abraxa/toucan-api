const handleSignout = (db) => (req, res) => {
  db('activities')
    .where({iduser: req.body.id, generated: true})
    .del()
    .then(data => {
      console.log('success');
      return res.json(data)
    })
    .catch(err => res.status(400).json('signout error'))
}

module.exports = {
  handleSignout
}