const handleSignin = (db, bcrypt, moment, _) => (req, res) => {
  db.select('id', 'username', 'password').from('users')
    .where('username', '=', req.body.username)
    .then(data => {
      console.log('data', data[0], req.body.password);
      const isValid = bcrypt.compareSync(req.body.password, data[0].password);
      if (isValid == true) {
        for(let i = 1; i < 51; i++){
          const randomDayWeek = _.random(6);
          console.log('random day week', randomDayWeek);
          const dayAct = moment().day(randomDayWeek);
          console.log('dayact', dayAct);
          const timeType = _.random(2);
          console.log('timeType', timeType);
          const maxInitTime = timeType == 0 ? 1800 : (timeType == 1 ? 3600 : 7200)
          const minInitTime = timeType == 0 ? 300 : (timeType == 1 ? 1860 : 3660)
          const initTime = _.random(minInitTime, maxInitTime)
          const maxRestTime = _.round(initTime * _.random(0, .20))
          const restTime = _.random(maxRestTime);
          console.log('restTime', restTime);
          db.transaction(trx => {
            trx.insert({
              iduser: data[0].id,
              name: "data " + i,
              timetype: timeType,
              status: 2,
              inittime: initTime,
              resttime: restTime,
              dateact: dayAct,
              generated: true
            })
            .into('activities')
            .then(trx.commit)
            .catch(trx.rollback)
          })
        }
        return res.json(data[0])
      } else {
        res.status(400).json('wrong credentials')
      }
    })
    .catch(err => res.status(400).json('wrong credentials'))
}

module.exports = {
  handleSignin
}