const handleGetUsers = (db, crypt) => (req, res) => {
  db.select('*').from('users')
    .then(data => {
        return res.json(data)
    })
    .catch(err => res.status(400).json('error in conecction'))
}

module.exports = {
  handleGetUsers
}