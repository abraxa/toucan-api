const express = require('express');
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt-nodejs');
const cors = require('cors');
const knex = require('knex');
const moment = require('moment');
const _ = require('lodash')

const signin = require('./controllers/signin/signin');
const register = require('./controllers/register/register');
const signout = require('./controllers/signout/signout');
const users = require('./controllers/users/users');
const activities = require('./controllers/activities/activities');

const db = knex({
  client: 'pg',
  connection: {
    host : '127.0.0.1',
    user : 'masko',
    password : '',
    database : 'toucan'
  }
});

const app = express();

app.use(cors())
app.use(bodyParser.json());

app.get('/', (req, res)=> { res.send(database.users); })

app.get('/users', users.handleGetUsers(db));

app.post('/activitiesdel', activities.handleDeleteActivitiesWhere(db))
app.get('/activities/:id', activities.handleGetActivitiesId(db))
app.get('/activities', activities.handleGetActivities(db))
app.get('/activities/list/:iduser', activities.handleGetActivitiesWhere(db))
app.post('/activities', activities.handlePostActivities(db))
app.put('/activities', activities.handlePutActivities(db, moment))

app.post('/register', register.handleRegister(db, bcrypt))

app.post('/signin', signin.handleSignin(db, bcrypt, moment, _))
app.post('/signout', signout.handleSignout(db))

app.listen(3000, ()=> {
  console.log('app is running on port 3000');
})